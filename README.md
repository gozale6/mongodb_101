## README - Tarea 1: Manipulación de Calificaciones de Estudiantes

En esta tarea, se trabajarán con calificaciones de estudiantes utilizando MongoDB. Asegúrate de seguir los siguientes pasos para completar la tarea.

### Paso 1: Descargar el archivo de calificaciones

1. Descarga el archivo "grades.json" que contiene las calificaciones de los estudiantes.

### Paso 2: Importar el archivo en MongoDB

2. Abre una terminal y ejecuta el siguiente comando para importar el archivo "grades.json" en una base de datos llamada "students" y una colección llamada "grades":
   ```
   $ mongoimport -d students -c grades < grades.json
   ```

### Paso 3: Confirmar la importación

3. Abre la terminal de MongoDB y confirma que la colección se importó correctamente con los siguientes comandos:
   ```
   > use students;
   > db.grades.count()
   ```

   - ¿Cuántos registros arrojó el comando count?: 800

### Paso 4: Encontrar las calificaciones de un estudiante

4. Encuentra todas las calificaciones del estudiante con el ID número 4.
   ```
   > db.grades.find({"student_id": 4}).pretty()
   ```

   - Resultado: quiz:27.29006335059361, homework:28.656451042441, exam:87.89071881934647, homework:5.244452510818443

### Paso 5: Contar registros de tipo exam

5. ¿Cuántos registros hay de tipo "exam"?
   ```
   > db.grades.find({ "type":"exam"}).count()
   ```

   - Resultado: 200

### Paso 6: Contar registros de tipo homework

6. ¿Cuántos registros hay de tipo "homework"?
   ```
   > db.grades.find({ "type":"homework"}).count()
   ```

   - Resultado: 400

### Paso 7: Contar registros de tipo quiz

7. ¿Cuántos registros hay de tipo "quiz"?
   ```
   > db.grades.find({ "type":"quiz"}).count()
   ```

   - Resultado: 200

### Paso 8: Eliminar las calificaciones de un estudiante

8. Elimina todas las calificaciones del estudiante con el ID número 3.
   ```
   > db.grades.remove({ "student_id": 3})
   ```

   - Resultado: { acknowledged: true, deletedCount: 4 }

### Paso 9: Encontrar estudiantes con una calificación específica

9. ¿Qué estudiantes obtuvieron una calificación de 75.29561445722392 en una tarea?
   ```
   > db.grades.find({ "score": 75.29561445722392 })
   ```

   - Resultado: [{_id: ObjectId("50906d7fa3c412bb040eb59e"),student_id: 9,type: 'homework',score: 75.29561445722392}]

### Paso 10: Actualizar una calificación por UUID

10. Actualiza las calificaciones del registro con el UUID "50906d7fa3c412bb040eb591" y asigna un valor de 100.
   ```
   > db.grades.updateOne({ "_id": ObjectId("50906d7fa3c412bb040eb591") }, { $set: { "score": 100 } })
   ```

   - Resultado: {acknowledged: true,insertedId: null,matchedCount: 1,modifiedCount: 1,upsertedCount: 0}

### Paso 11: Encontrar el estudiante asociado a una calificación

11. ¿A qué estudiante pertenece la calificación con el UUID "50906d7fa3c412bb040eb591"?
   ```
   > db.grades.findOne({ "_id": ObjectId("50906d7fa3c412bb040eb591") }).student_id
   ```

   - Resultado: 6

## Autor
- Nombre: Alejandro Arturo Gonzalez Flores
- Correo electrónico: a348552@uach.mx

## Licencia
001
